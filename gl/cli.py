from __future__ import annotations

import argparse
from typing import Sequence

from . import __version__


def main(argv: Sequence[str] | None = None) -> int:
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--version", "-V", action="version", version=__version__)
    arg_parser.parse_args(argv)
    return 0


if __name__ == "__main__":
    raise SystemExit(main())
