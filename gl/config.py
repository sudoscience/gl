import os
import re
from configparser import ExtendedInterpolation, InterpolationMissingOptionError
from typing import Mapping, MutableMapping


class ExtendedEnvInterpolation(ExtendedInterpolation):
    def before_get(
        self,
        parser: MutableMapping[str, Mapping[str, str]],
        section: str,
        option: str,
        value: str,
        default: Mapping[str, str],
    ) -> str:
        try:
            v = super().before_get(parser, section, option, value, default)
        except InterpolationMissingOptionError:
            m = re.match(r"\${([^:]+)(?::([^}]*))?}", value)
            assert m is not None  # nosec
            groups = m.groups()
            v = os.environ[groups[0]] if groups[1] is None else os.getenv(*groups)
        return v
