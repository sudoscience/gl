# See https://diyan.github.io/posts/2015/all_pythons_in_docker/
FROM ubuntu:18.04
RUN set -x \
    && pythonVersions='python3.8 python3.9 python3.10' \
    && apt-get update \
    && apt-get install -y --no-install-recommends software-properties-common \
    && apt-add-repository -y 'ppa:deadsnakes/ppa' \
    && apt-get update \
    && apt-get install -y --no-install-recommends $pythonVersions python3.9-venv \
    && apt-get purge -y --auto-remove software-properties-common \
    && rm -rf /var/lib/apt/lists/*
CMD bash
