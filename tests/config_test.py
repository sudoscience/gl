import os
from configparser import ConfigParser
from unittest import mock

import pytest

from gl import config

CFG = """
[test_section]
test_key = test_value
test_var = ${SOME_VAR:default_value}
test_other_var = ${SOME_OTHER_VAR}
"""

BAD_CFG = """
[test_section]
bad_var = ${UNDEFINED_VAR}
"""


def test_cfg(cfg):
    cfg.read_string(CFG)
    assert cfg["test_section"]["test_key"] == "test_value"
    assert cfg["test_section"]["test_var"] == "default_value"


@mock.patch.dict(
    os.environ, {"SOME_VAR": "some_value", "SOME_OTHER_VAR": "some_other_value"}
)
def test_cfg_from_env(cfg):
    cfg.read_string(CFG)
    assert cfg["test_section"]["test_var"] == "some_value"
    assert cfg["test_section"]["test_other_var"] == "some_other_value"


def test_cfg_crashes_for_undefined_envvar(cfg):
    cfg.read_string(BAD_CFG)
    with pytest.raises(KeyError):
        cfg["test_section"]["bad_var"]


@pytest.fixture
def cfg():
    return ConfigParser(interpolation=config.ExtendedEnvInterpolation())
