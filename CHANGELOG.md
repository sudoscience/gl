# Changelog

<!--next-version-placeholder-->

## v0.1.0 (2022-06-28)
### Feature
* Initial commit ([`502a067`](https://gitlab.com/sudoscience/gl/-/commit/502a067c6c5a219b8eb894cbe1a433f220569194))
