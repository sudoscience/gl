import pytest

import gl.cli


def test_version(capsys):
    with pytest.raises(SystemExit):
        gl.cli.main(["--version"])
    out, err = capsys.readouterr()
    assert out == f"{gl.__version__}\n"
    assert err == ""


def test_no_op():
    assert gl.cli.main([]) == 0
